package cat.gergokajtar.nicequesttest.feature.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.view.doOnLayout
import cat.gergokajtar.nicequesttest.R
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import com.squareup.picasso.Picasso

class GalleryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val itemImageView: ImageView
    val titleTextView: TextView
    val viewsTextView: TextView

    val viewsStringFormat: String

    init {
        itemImageView = itemView.findViewById(R.id.imageview_item_image)
        titleTextView = itemView.findViewById(R.id.textview_item_title)
        viewsTextView = itemView.findViewById(R.id.textview_views)

        viewsStringFormat = itemView.context.resources.getString(R.string.views_format)
    }

    fun bind(galleryItem: GalleryItem) {
        when {
            galleryItem.title?.isNotBlank() == true -> {
                titleTextView.text = galleryItem.title
                titleTextView.visibility = View.VISIBLE
            }
            else -> titleTextView.visibility = View.GONE
        }

        viewsTextView.text = String.format(viewsStringFormat, galleryItem.views)
        if (itemImageView.width > 0) {
            loadImage(galleryItem.imageUrl)
        } else {
            itemImageView.doOnLayout {
                loadImage(galleryItem.imageUrl)
            }
        }
    }

    private fun loadImage(imageUrl: String) {
        Picasso.with(itemView.context)
                .load(imageUrl)
                .resize(itemImageView.width, itemImageView.height)
                .centerCrop()
                .into(itemImageView)
    }
}