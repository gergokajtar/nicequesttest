package cat.gergokajtar.nicequesttest.feature.list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cat.gergokajtar.nicequesttest.R
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem

class GalleryItemAdapter : RecyclerView.Adapter<GalleryItemViewHolder>() {

    var galleryItems: MutableList<GalleryItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_gallery_item, parent, false)
        return GalleryItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GalleryItemViewHolder, position: Int) {
        holder.bind(galleryItems[position])
    }

    override fun getItemCount(): Int = galleryItems.size

    fun setItems(newItems: List<GalleryItem>) {
        val diffResults = DiffUtil.calculateDiff(GalleryItemListDiffUtil(galleryItems, newItems))

        galleryItems.clear()
        galleryItems.addAll(newItems)

        diffResults.dispatchUpdatesTo(this)
    }

    class GalleryItemListDiffUtil(val oldList: List<GalleryItem>, val newList: List<GalleryItem>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].imageUrl.equals(newList[newItemPosition].imageUrl)
        }

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
                areItemsTheSame(oldItemPosition, newItemPosition)
    }
}