package cat.gergokajtar.nicequesttest.feature.list

import android.content.Context
import android.content.Intent
import cat.gergokajtar.nicequesttest.feature.login.LoginActivity

class ListIntent(packageContext: Context) : Intent(packageContext, ListActivity::class.java)