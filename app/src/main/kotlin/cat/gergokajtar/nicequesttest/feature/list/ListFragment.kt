package cat.gergokajtar.nicequesttest.feature.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cat.gergokajtar.nicequesttest.R
import cat.gergokajtar.nicequesttest.auth.TokenRepository
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import cat.gergokajtar.nicequesttest.di.Injectable
import cat.gergokajtar.nicequesttest.feature.login.LoginIntent
import javax.inject.Inject

class ListFragment : Fragment(), Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var tokenRepository: TokenRepository
    lateinit var viewModel: GalleryItemsViewModel

    lateinit var recyclerView: RecyclerView
    lateinit var galleryItemAdapter: GalleryItemAdapter

    lateinit var scrollListener: RecyclerView.OnScrollListener

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (tokenRepository.accessToken.isNullOrEmpty()) {
            startActivity(LoginIntent(context!!))
            activity?.finish()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(GalleryItemsViewModel::class.java)

        startObserveItems()
        startObserveLoading()
        startObserveError()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_list, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(R.id.recyclerview_images)

        recyclerView.layoutManager = GridLayoutManager(context, 2)
        galleryItemAdapter = GalleryItemAdapter()
        recyclerView.adapter = galleryItemAdapter
        scrollListener = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                with (this@ListFragment.recyclerView.layoutManager as GridLayoutManager) {
                    val visibleItemCount = childCount
                    val totalItemCount = itemCount
                    val firstVisibleItemPosition = findFirstVisibleItemPosition()

                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount) {
                        this@ListFragment.recyclerView.removeOnScrollListener(scrollListener)
                        viewModel.loadNextPage()
                    }
                }

            }

        }
    }

    private fun startObserveLoading() {
        viewModel.loading.observe(this,
                Observer { loading ->
                    if (loading == true) {
                        Snackbar.make(view!!, "Loading data", Snackbar.LENGTH_SHORT).show()
                    }
                })
    }

    private fun startObserveItems() {
        viewModel.itemsToShow.observe(this,
                Observer {
                    val newEvents = when {
                        it == null -> emptyList<GalleryItem>()
                        else -> it
                    }
                    galleryItemAdapter.setItems(newEvents)
                    recyclerView.addOnScrollListener(scrollListener)
                })
    }

    private fun startObserveError() {
        viewModel.error.observe(this,
                Observer { error ->
                    if (error != null) {
                        val mySnackbar = Snackbar.make(view!!,
                                "There has been a problem.", Snackbar.LENGTH_INDEFINITE)
                        mySnackbar.setAction("Retry", {
                            mySnackbar.dismiss()
                            viewModel.refresh()
                        })
                        mySnackbar.show()
                    }
                })
    }
}