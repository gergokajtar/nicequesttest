package cat.gergokajtar.nicequesttest.feature.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import cat.gergokajtar.nicequesttest.BuildConfig
import cat.gergokajtar.nicequesttest.R
import cat.gergokajtar.nicequesttest.auth.ImgurLoginRedirectUri
import cat.gergokajtar.nicequesttest.auth.LoginRedirectUri
import cat.gergokajtar.nicequesttest.auth.TokenRepository
import cat.gergokajtar.nicequesttest.feature.list.ListIntent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    @Inject lateinit var tokenRepository: TokenRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val loginButton = findViewById<Button>(R.id.button_login)
        loginButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.IMGUR_LOGIN_URL))
            startActivity(intent)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        val imgurUri: LoginRedirectUri = ImgurLoginRedirectUri(this, intent.data)

        imgurUri.check(
                { accessToken, refreshToken ->
                    tokenRepository.accessToken = accessToken
                    tokenRepository.refreshToken = refreshToken
                    startActivity(ListIntent(this@LoginActivity))
                    finish()
                },
                {
                    //TODO Ignore the error for now
                })


    }
}