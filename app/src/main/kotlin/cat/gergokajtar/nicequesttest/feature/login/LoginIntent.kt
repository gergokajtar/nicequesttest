package cat.gergokajtar.nicequesttest.feature.login

import android.content.Context
import android.content.Intent


class LoginIntent(packageContext: Context) : Intent(packageContext, LoginActivity::class.java)
