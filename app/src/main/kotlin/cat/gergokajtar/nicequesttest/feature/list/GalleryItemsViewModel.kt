package cat.gergokajtar.nicequesttest.feature.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import cat.gergokajtar.nicequesttest.data.repository.GalleryItemRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GalleryItemsViewModel @Inject constructor(val imageRepository: GalleryItemRepository) : ViewModel() {

    var itemsToShow = MutableLiveData<List<GalleryItem>>()
    private var galleryItems: MutableList<GalleryItem> = ArrayList()
    var imagesObserver: Disposable? = null

    var loading = MutableLiveData<Boolean>()
    var error = MutableLiveData<Throwable>()

    // Just for the sake of the demo it is hardcoded, normally it comes from an "advanced search"
    val sorting: String = "top"
    val query: String = "title:cats ext:jpg"

    var nextPageToLoad: Int = 1

    var successConsumer: Consumer<List<GalleryItem>> = Consumer {
        loading.postValue(false)
        if (nextPageToLoad == 1) {
            galleryItems.clear()
        }
        galleryItems.addAll(it)

        itemsToShow.postValue(galleryItems)
        nextPageToLoad++
        imagesObserver?.dispose()
    }

    var errorConsumer: Consumer<Throwable> = Consumer {
        loading.postValue(false)
        error.postValue(it)
        imagesObserver?.dispose()
    }

    init {
        loadNextPage()
    }

    fun refresh() {
        if (imagesObserver?.isDisposed ?: true) {
            reset()
            loadNextPage()
        }
    }

    fun loadNextPage() {
        if (loading.value == true) return

        loading.value = true
        imagesObserver = imageRepository.searchGallery(sorting, nextPageToLoad, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer, errorConsumer)
    }

    private fun reset() {
        nextPageToLoad = 1
    }

    override fun onCleared() {
        imagesObserver?.dispose()
    }
}