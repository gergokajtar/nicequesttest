package cat.gergokajtar.nicequesttest.auth

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.content.edit
import javax.inject.Inject

class TokenRepositoryImpl @Inject constructor(app: Application) : TokenRepository {

    private val sharedPrefs: SharedPreferences

    init {
        sharedPrefs = app.getSharedPreferences(NICEQUEST_SHAREDPREFS, Context.MODE_PRIVATE)
    }

    override var accessToken: String
        get() = sharedPrefs.getString(ACCESS_TOKEN, "")
        set(value) {
            sharedPrefs.edit {
                putString(ACCESS_TOKEN, value)
            }
        }
    override var refreshToken: String
        get() = sharedPrefs.getString(REFRESH_TOKEN, "")
        set(value) {
            sharedPrefs.edit {
                putString(REFRESH_TOKEN, value)
            }
        }

    companion object {
        val NICEQUEST_SHAREDPREFS = "NicequestSharedPrefs"
        val ACCESS_TOKEN = "accessToken"
        val REFRESH_TOKEN = "refreshToken"
    }
}