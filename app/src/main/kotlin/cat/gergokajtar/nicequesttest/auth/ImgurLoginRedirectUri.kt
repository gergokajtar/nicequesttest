package cat.gergokajtar.nicequesttest.auth

import android.content.Context
import android.net.Uri
import cat.gergokajtar.nicequesttest.R

class ImgurLoginRedirectUri(val context: Context,
                            override val uri: Uri) : LoginRedirectUri {

    private val errorParam: String
    private val accessDeniedParamValue: String
    private val accessTokenParam: String
    private val refreshTokenParam: String

    private val workerLoginUri: Uri by lazy { uri.buildUpon()
            .encodedQuery(uri.fragment)
            .build() }

    init {
        errorParam = context.getString(R.string.error)
        accessDeniedParamValue = context.getString(R.string.access_denied)
        accessTokenParam = context.getString(R.string.access_token)
        refreshTokenParam = context.getString(R.string.refresh_token)
    }

    override fun check(onSuccess: (String, String) -> Unit, onError: (Throwable) -> Unit) {
        when {
            hasUserDeniedAccess() -> onError(Throwable(context.getString(R.string.error_login_access_denied)))
            hasUserLoggedIn() -> onSuccess(getAccessTokenFrom(), getRefreshTokenFrom())
        }
    }

    private fun hasUserDeniedAccess(): Boolean {
        return with (uri) {
            queryParameterNames.contains(errorParam) && getQueryParameter(errorParam).equals(accessDeniedParamValue)
        }
    }

    private fun hasUserLoggedIn(): Boolean {
        return with(workerLoginUri) {
            queryParameterNames.contains(accessTokenParam) && queryParameterNames.contains(refreshTokenParam)
        }
    }

    private fun getAccessTokenFrom(): String {
        return workerLoginUri.getQueryParameter(accessTokenParam)
    }

    private fun getRefreshTokenFrom(): String {
        return workerLoginUri.getQueryParameter(refreshTokenParam)
    }
}