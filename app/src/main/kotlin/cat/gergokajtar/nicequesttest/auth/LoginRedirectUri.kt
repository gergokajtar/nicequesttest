package cat.gergokajtar.nicequesttest.auth

import android.net.Uri

interface LoginRedirectUri {
    val uri: Uri

    fun check(onSuccess: (String, String) -> Unit, onError: (Throwable) -> Unit)
}