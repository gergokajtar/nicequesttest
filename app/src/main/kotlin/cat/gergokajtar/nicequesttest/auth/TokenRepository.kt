package cat.gergokajtar.nicequesttest.auth

interface TokenRepository {
    var accessToken: String
    var refreshToken: String
}