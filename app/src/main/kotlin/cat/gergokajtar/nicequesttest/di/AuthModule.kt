package cat.gergokajtar.nicequesttest.di

import android.app.Application
import cat.gergokajtar.nicequesttest.auth.*
import dagger.Module
import dagger.Provides

@Module
class AuthModule {

    @Provides
    fun provideTokenRepository(app: Application): TokenRepository = TokenRepositoryImpl(app)

}