package cat.gergokajtar.nicequesttest.di

import cat.gergokajtar.nicequesttest.feature.list.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun contributeListFragment(): ListFragment

}