package cat.gergokajtar.nicequesttest.di

import cat.gergokajtar.nicequesttest.data.repository.GalleryItemRepository
import cat.gergokajtar.nicequesttest.data.repository.GalleryItemRepositoryImpl
import cat.gergokajtar.nicequesttest.data.source.RemoteGalleryItemDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class GalleryModule {

    @Provides
    fun provideGalleryItemRepository(remoteGalleryItemDataSource: RemoteGalleryItemDataSource): GalleryItemRepository =
            GalleryItemRepositoryImpl(remoteGalleryItemDataSource)

    @Provides
    fun provideRemoteGalleryItemDataSource(retrofit: Retrofit): RemoteGalleryItemDataSource =
            retrofit.create(RemoteGalleryItemDataSource::class.java)
}