package cat.gergokajtar.nicequesttest.di

import cat.gergokajtar.nicequesttest.feature.list.ListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ListActivityModule {

    @ContributesAndroidInjector(modules = arrayOf(FragmentBuildersModule::class))
    abstract fun contributeListActivity(): ListActivity
}