package cat.gergokajtar.nicequesttest.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import cat.gergokajtar.nicequesttest.feature.list.GalleryItemsViewModel
import cat.gergokajtar.nicequesttest.viewmodel.GalleryItemsViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = arrayOf(GalleryModule::class, NetworkModule::class))
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(GalleryItemsViewModel::class)
    abstract fun bindGalleryItemsViewModel(galleryItemsViewModel: GalleryItemsViewModel): ViewModel

    @Binds
    abstract fun bindGalleryItemsViewModelFactory(factory: GalleryItemsViewModelFactory): ViewModelProvider.Factory
}
