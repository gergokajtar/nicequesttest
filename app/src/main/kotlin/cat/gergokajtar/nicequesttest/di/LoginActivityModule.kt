package cat.gergokajtar.nicequesttest.di

import cat.gergokajtar.nicequesttest.feature.list.ListActivity
import cat.gergokajtar.nicequesttest.feature.login.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginActivityModule {

    @ContributesAndroidInjector(modules = arrayOf(FragmentBuildersModule::class))
    abstract fun contributeLoginActivity(): LoginActivity
}