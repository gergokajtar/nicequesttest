package cat.gergokajtar.nicequesttest.data.model.mapper

import cat.gergokajtar.nicequesttest.data.model.json.JsonGallerySearchResponse
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import io.reactivex.functions.Function

class JsonGallerySearchResponseMapper : Function<JsonGallerySearchResponse, List<GalleryItem>> {
    override fun apply(jsonResponse: JsonGallerySearchResponse): List<GalleryItem> {
        val galleryItems: MutableList<GalleryItem> = ArrayList()
        jsonResponse.data.forEach { jsonGalleryItem ->
            when (jsonGalleryItem.isAlbum) {
                true -> galleryItems.addAll(jsonGalleryItem.images.map { GalleryItem(it.title, it.link, it.views) })
                false -> galleryItems.add((GalleryItem(jsonGalleryItem.title, jsonGalleryItem.link, jsonGalleryItem.views)))
            }
        }

        return galleryItems
    }
}