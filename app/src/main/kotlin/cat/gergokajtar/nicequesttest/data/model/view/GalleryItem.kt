package cat.gergokajtar.nicequesttest.data.model.view

data class GalleryItem(var title: String?,
                       var imageUrl: String,
                       var views: Long) {
}