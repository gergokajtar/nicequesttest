package cat.gergokajtar.nicequesttest.data.source

import cat.gergokajtar.nicequesttest.BuildConfig
import cat.gergokajtar.nicequesttest.data.model.json.JsonGallerySearchResponse
import io.reactivex.Single
import retrofit2.http.*

interface RemoteGalleryItemDataSource {

    @Headers(value = "Authorization: Client-ID " + BuildConfig.CLIENT_ID)
    @GET("gallery/search/{sort}/{page}")
    fun searchGallery(@Path("sort") sorting: String,
                         @Path("page") nextPage: Int,
                         @Query("q") query: String): Single<JsonGallerySearchResponse>

}