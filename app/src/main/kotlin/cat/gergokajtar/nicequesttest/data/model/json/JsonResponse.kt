package cat.gergokajtar.nicequesttest.data.model.json
import com.google.gson.annotations.SerializedName


data class JsonGallerySearchResponse(
		@SerializedName("data") var data: List<JsonGalleryItem>
)

data class JsonGalleryItem(
		@SerializedName("title") var title: String,
		@SerializedName("views") var views: Long,
		@SerializedName("link") var link: String,
		@SerializedName("is_album") var isAlbum: Boolean,
		@SerializedName("images") var images: List<JsonAlbumImage>
)

data class JsonAlbumImage(
		@SerializedName("title") var title: String?,
		@SerializedName("views") var views: Long,
		@SerializedName("link") var link: String
)