package cat.gergokajtar.nicequesttest.data.repository

import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import io.reactivex.Single

interface GalleryItemRepository {
    fun searchGallery(sorting: String, nextPage: Int, query: String): Single<List<GalleryItem>>
}