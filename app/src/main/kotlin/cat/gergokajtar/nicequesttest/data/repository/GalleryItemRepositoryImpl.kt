package cat.gergokajtar.nicequesttest.data.repository

import cat.gergokajtar.nicequesttest.data.model.mapper.JsonGallerySearchResponseMapper
import cat.gergokajtar.nicequesttest.data.model.view.GalleryItem
import cat.gergokajtar.nicequesttest.data.source.RemoteGalleryItemDataSource
import io.reactivex.Single
import javax.inject.Inject

class GalleryItemRepositoryImpl @Inject constructor(val remoteGalleryItemDataSource: RemoteGalleryItemDataSource) : GalleryItemRepository {

    override fun searchGallery(sorting: String, nextPage: Int, query: String): Single<List<GalleryItem>> {
        return remoteGalleryItemDataSource.searchGallery(sorting, nextPage, query)
                .map { jsonResponse -> JsonGallerySearchResponseMapper().apply(jsonResponse)}
    }
}